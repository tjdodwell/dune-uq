#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include<math.h>
#include<iostream>
#include<vector>
#include<map>
#include<string>
#include<sstream>
#include <mpi.h>

#include <config.h>


#include <dune/common/parametertree.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/grid/yaspgrid.hh> // Checked Inclusion
#include <dune/grid/common/gridview.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/common/gridinfo.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/istl/io.hh>
#include <dune/istl/matrixmarket.hh>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parallel/collectivecommunication.hh>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include<dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/io.hh>
#include <dune/istl/superlu.hh>




#include <dune/pdelab/newton/newton.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/finiteelementmap/pkfem.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/localfunctions/lagrange/qk.hh>
#include <dune/pdelab/finiteelementmap/rannacherturekfem.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/genericdatahandle.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/instationary/onestep.hh>
#include <dune/pdelab/common/instationaryfilenamehelper.hh>
#include <dune/pdelab/instationary/implicitonestep.hh>

#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include <dune/pdelab/backend/istlsolverbackend.hh>
#include <dune/pdelab/stationary/linearproblem.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/gridoperator/onestep.hh>
#include <dune/pdelab/instationary/onestepparameter.hh>

#include<dune/pdelab/adaptivity/adaptivity.hh>

#include "user_inputs/user_model_diffusion.hh"
#include "UQ_functions/STD_MLMC.hh"



int main(int argc, char** argv)
{
  try{
    // Maybe initialize MPI
      
      MPI_Init(&argc,&argv);
      
      std::string gridName = "grids/test.msh";
      
      std::cout << "Here " << std::endl;
      
      /*
      
      int rank;
      
      MPI_Comm_rank(MPI_COMM_WORLD, &rank);
      
      
      MPI_Comm newcomm;
      
      MPI_Comm_split(MPI_COMM_WORLD, rank, 0, &newcomm);
   
      const int dim = 3;
      
      Dune::FieldVector<double,dim> L(1.0);

      
     // Make grid using UGGrid
      typedef Dune::UGGrid<3> UGGRID;
      UGGRID grid;
      Dune::GridFactory<UGGRID> factory(&grid);
      Dune::GmshReader<UGGRID>::read(factory,gridName);
      factory.createGrid();
      //grid.loadBalance(newcomm);
      const int dofel = 12;
      
      
      
      typedef MODEL<Dune::UGGrid<dim>> MY_MODEL;
      
      MY_MODEL model(grid,L);
      
      STD_MLMC<MY_MODEL> mlmc(model);
      
      mlmc.setParameter(0.005,0,1000,1);
      
      mlmc.apply();

      */
      
      //MPI_Finalize();
      
    return 0;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
    
 
    
}
