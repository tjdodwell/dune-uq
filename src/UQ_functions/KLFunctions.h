//
//  KLFunctions.h
//  
//
//  Created by Tim Dodwell on 01/12/2015.
//
//

#ifndef KLFunctions_h
#define KLFunctions_h

// ------------------------------------------------------------------------------------
// Define K-L Expansion
// ------------------------------------------------------------------------------------

double residual(double omega, double L, double c, int i){
    double f = 0.0;
    
    
    if  (((i+1) % 2) == 1){
        f = omega * tan(omega * L) - c;

    }
    else {
        f = omega + c * tan(omega * L);
    }
    return f;
}

template<typename VEC>
void KLExpansion(int numMode1D, double L, double sig, double ell, VEC& freq)
{
    
    double tol = 1e-6;
    double error, a, b, c1, c, fa, fb, fc, m;
    
    // For each mode
    for (int i = 0; i < numMode1D; i++){
        
        error = 2.0 * tol;
        if (i == 0){ a = 0;}
        else {a = (1 / L) * (i - 0.499) * M_PI;}
        b = (1 / L) * (i + 0.499) * M_PI;

        
        while (error  > tol)
        {
            fa = residual(a,L,1/ell,i);
            fb = residual(b,L,1/ell,i);
            
            m = (fb - fa) / (b - a);
            
            c = a - fa / m;
            
            fc = residual(c,L,1/ell,i);
            
            if (((fa < 0) & (fc < 0)) | ((fa > 0) & (fc > 0))) { a = c; }
            else { b = c; }
            
            error = fc;
            
        }
        
        freq[i] = c;
    }
}


double evaluate_eigenFunctions(double x, int i, double omega)
{
    double u = 0.0;
    if (i % 2 == 0){
        u = std::cos(omega * (x - 0.5));
        u /= sqrt(0.5 + 0.5 * (std::sin(omega) / omega));
    }
    else{
        u = std::sin(omega * (x - 0.5));
        u /= sqrt(0.5 - 0.5 * (std::sin(omega) / omega));
    }
    return u;
}

template<class X>
void inline evaluate_eigenValues(double ell, X& lambda, X& freq){
    double c = 1 / ell;
    for (int i = 0; i < lambda.size(); i++){
        lambda[i] = 2.0 * c /(freq[i] * freq[i] + c * c);
    }
}

template<class X,class Y>
void bubbleSort(X& index, Y& lam)
{
    const int N = lam.size();
    
    for (int i = 0; i < N; i++){ index[i] = i; }
    
    int sw = 1; double tmpreal;
    int tmpint;
    
    while (sw == 1)
    {
        sw = 0;
        for(int i = 0; i < N-1; i++)
        {
            if (lam[i] < lam[i+1])
            {
                tmpreal = lam[i+1];
                tmpint = index[i+1];
                lam[i+1] = lam[i];
                index[i+1] = index[i];
                lam[i] = tmpreal;
                index[i] = tmpint;
                sw = 1;
            }
        }
    }
}

template<class X, class Y, class Z>
void inline construct_3D_eigenValues(Z& lam1D,X& lambda3D, Y& id2d_i, Y& id2d_j, Y& id2d_k){
    const int N = lam1D.size();
    
    std::vector<int> index_i(N * N * N), index_j(N * N * N), index_k(N * N * N);
    std::vector<double> lam3D(lam1D.size() * lam1D.size() * lam1D.size());
    std::vector<int> ind(lam1D.size() * lam1D.size() * lam1D.size());
    
    int counter = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++){

            for (int k = 0; k < N; k++){
            
                lam3D[counter] = lam1D[i] * lam1D[j] * lam1D[k];
                
                ind[counter] = counter;
                
                index_i[counter] = i;
                index_j[counter] = j;
                index_k[counter] = k;
            
                counter += 1;

            }
            
        }
    }

    bubbleSort(ind,lam3D);
    
    for (int i = 0; i < lambda3D.size(); i++){
        id2d_i[i] = index_i[ind[i]];
        id2d_j[i] = index_j[ind[i]];
        id2d_k[i] = index_k[ind[i]];
        lambda3D[i] = lam3D[i];
    
    }

}




template<class X, class Y, class Z>
void inline construct_2D_eigenValues(Z& lam1D,X& lambda2D, Y& id2d_i, Y& id2d_j){
    const int N = lam1D.size();
    
    std::vector<int> index_i(N * N),    index_j(N * N);
    std::vector<double> lam2D(lam1D.size() * lam1D.size());
    std::vector<int> ind(lam1D.size() * lam1D.size());
    
    int counter = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++){
            
            lam2D[counter] = lam1D[i] * lam1D[j];
            
            ind[counter] = counter;
            
            index_i[counter] = i;
            index_j[counter] = j;
        
            counter += 1;
            
        }
    }

    bubbleSort(ind,lam2D);
    
    for (int i = 0; i < lambda2D.size(); i++){
        id2d_i[i] = index_i[ind[i]];
        id2d_j[i] = index_j[ind[i]];
        lambda2D[i] = lam2D[i];
    
    }

}




#endif /* KLFunctions_h */
