//
//  random_field.hh
//  
//
//  Created by Tim Dodwell on 30/11/2015.
//
//

#ifndef random_field_h
#define random_field_h

#include <random>
#include "./UQ_functions/general.hh"

template <typename RAND_COEFF, typename MODEL>
RAND_COEFF random_field_mlmc(MODEL& model,RAND_COEFF& z)
{
// This is a user defined function which defines the random coefficients for a single realisation of model
    
    RAND_COEFF z(model.numPlys,model.numKLmodes);
    
    int numPlys = model.getnumPlys();
    
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> randu(0.0, 1.0);
    std::normal_distribution<double> randn(0.0,model.sigma);
    
    for (int i = 0; i < numPlys; i++){
        if (randu(gen) < model.defectProbability){
            z.setPlyDefect(i,true);
            for (int j = 0; j < model.KLmodes()){
                z.setKLcoeff(i,j,randn(gen));
            }
        }
        else{ z.setPlyDefect(i,false); }
    }
    
    return z;
    
}






#endif /* random_field_h */
