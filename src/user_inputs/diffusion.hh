// -*- tab-width: 4; c-basic-offset: 2; indent-tabs-mode: nil -*-

#ifndef DUNE_PDELAB_DIFFUSION_HH
#define DUNE_PDELAB_DIFFUSION_HH

#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>

#include "defaultimp.hh"
#include "pattern.hh"
#include "flags.hh"
#include "idefault.hh"

namespace Dune {
    namespace PDELab {
        
        template<typename LE, typename GV>
        class diffuse : public NumericalJacobianApplyVolume<diffuse<LE,GV>>,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<diffuse<LE,GV> >
        {
        public:
            // pattern assembly flags
            enum { doPatternVolume = true };
            
            // residual assembly flags
            enum { doAlphaVolume = true };
            
            diffuse (GV& gv_, LE& le_, int intorder_=2)
            : le(le_), intorder(intorder_), gv(gv_)
            {}
            
            // volume integral depending on test and ansatz functions
            template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
            void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
            {
                // dimensions
                const int dim = 3;
                const int dimw = 3;
                
                const unsigned int nodel = lfsu.size();
                
                // domain and range field type
                typedef typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::DomainFieldType DF;
                typedef typename R::value_type RF;
                typedef typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;
                typedef typename LFSU::Traits::SizeType size_type;
                
                typedef typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::RangeType RangeType;
                
                // select quadrature rule
                GeometryType gt = eg.geometry().type();
                const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,intorder);
                
                // Evaluate elasticity tensor (assumes it is constant over a single element)
                Dune::FieldMatrix<double,3,3> Kij(0.0);

                // Unwrap solution for element
                FieldVector<double,4> p(0.0);
                for (int i=0; i < lfsu.size(); i++){
                    p[i] = x(lfsu,i);
                }
                
                const typename GV::IndexSet& is(gv.indexSet());
                
                le.template evaluateTensor(is.index(eg.entity()),Kij);
                
                // Loop over quadrature points
                for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
                {
                    
                    // Evaluate Jacobian
                    std::vector<JacobianType> js(nodel);
                    lfsu.finiteElement().localBasis().evaluateJacobian(it->position(),js);
                    
                    // Transform gradient to real element
                    const typename EG::Geometry::JacobianInverseTransposed jac = eg.geometry().jacobianInverseTransposed(it->position());
                    std::vector<Dune::FieldVector<RF,dim> > gradphi(nodel);
                    
                    for (int i=0; i < lfsu.size(); i++)
                    {
                        gradphi[i] = 0.0;
                        jac.umv(js[i][0],gradphi[i]);
                    }
                    
                    Dune::FieldMatrix<double,dim,4> G(0.0);
                    
                    for (int i = 0; i < nodel; i++){
                        G[0][i] = gradphi[i][0];
                        G[1][i] = gradphi[i][1];
                        G[2][i] = gradphi[i][2];
                    }
                    
                    Dune::FieldVector<double,dim> gradp(0.0),flux(0.0);
                    
                    G.mv(p,gradp); // compute pressure gradient  grad(p) = gradphi * p
                    
                    Kij.mv(gradp,flux); // Compute flux = - Perm * G
                    
                    Dune::FieldVector<double,4> residual(0.0);
                    
                    G.mtv(flux,residual); // Compute residual vector res = - G' * Perm * G * p
                          
                    Dune::FieldVector<double,dim> x_global(0.0);
                    
                    // evaluate basis functions
                    std::vector<RangeType> phi(lfsu.size());
                    lfsu.finiteElement().localBasis().evaluateFunction(it->position(),phi);
                    
                    for (int i = 0; i < 4; i++){
                        x_global += eg.geometry().corner(i) * phi[i];
                    }
                    
                    double f = le.evaluateF(x_global,false);
                    
                    RF factor = it->weight() * eg.geometry().integrationElement(it->position());
                    
                    for (int i=0; i < nodel; i++){
                        r.accumulate(lfsv,i,(residual[i] - f * phi[i]) * factor);
                    }
                    
                    
                } // end for each quadrature point
                
            } // end alpha_volume
            
            
        private:
            
            const LE& le;
            const GV& gv;
            int intorder;
            
        };
        
        
        template<typename LE, typename V, typename GV, typename GFS>
        class errorEstimator : public NumericalJacobianApplyVolume<errorEstimator<LE,V,GV,GFS>>,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<errorEstimator<LE,V,GV,GFS> >
        {
        public:
            // pattern assembly flags
            enum { doPatternVolume = true };
            
            // residual assembly flags
            enum { doAlphaVolume = true };
            
            errorEstimator (LE& le_,GV& gv_, V& pc_, GFS& gfs_, int intorder_=2)
            : le(le_),pc(pc_), intorder(intorder_), gv(gv_), gfs(gfs_)
            {}
            
            // volume integral depending on test and ansatz functions
            template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
            void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
            {
                // dimensions
                const int dim = 3;
                const int dimw = 3;
                
                
                const typename GV::IndexSet& is(gv.indexSet());
                
                typedef Dune::PDELab::LocalFunctionSpace<GFS> CLFS;
                CLFS clfs(gfs);
                
                typedef Dune::PDELab::LFSIndexCache<CLFS> CLFSCache;
                CLFSCache clfsCache(clfs);
                std::vector<double> lp_c(clfs.maxSize());
                
                
                typedef typename V::template ConstLocalView<CLFSCache> VView;
                VView xcView(pc);
                
                
                // Bind solution x to local element
                clfs.bind(eg.entity());
                clfsCache.update();
                
                xcView.bind(clfsCache);
                xcView.read(lp_c);
                
                
                const unsigned int nodes_per_element = lfsu.size();
                
                // domain and range field type
                typedef typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::DomainFieldType DF;
                typedef typename R::value_type RF;
                typedef typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;
                typedef typename LFSU::Traits::SizeType size_type;
                
                typedef typename LFSU::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::RangeType RangeType;
                
                
                // select quadrature rule
                GeometryType gt = eg.geometry().type();
                const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,intorder);
                
                Dune::FieldVector<double,4> e(0.0);
                
                for (int i = 0; i < nodes_per_element; i++){
                    e[i] = x(lfsu,i) - lp_c[i];
                }
                
                // Evaluate elasticity tensor (assumes it is constant over a single element)
                Dune::FieldMatrix<double,3,3> Kij(0.0);
                
                le.template evaluateTensor(is.index(eg.entity()),Kij);
                
                // Loop over quadrature points
                for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
                {
                    
                    // Evaluate Jacobian
                    std::vector<JacobianType> js(lfsu.size());
                    lfsu.finiteElement().localBasis().evaluateJacobian(it->position(),js);
                    
                    // Transform gradient to real element
                    const typename EG::Geometry::JacobianInverseTransposed jac = eg.geometry().jacobianInverseTransposed(it->position());
                    std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
                    
                    for (int i=0; i < lfsu.size(); i++)
                    {
                        gradphi[i] = 0.0;
                        jac.umv(js[i][0],gradphi[i]);
                    }
                    
                    Dune::FieldMatrix<double,dim,4> G(0.0);
                    
                    for (int i = 0; i < lfsu.size(); i++){
                        G[0][i] = gradphi[i][0];
                        G[1][i] = gradphi[i][1];
                        G[2][i] = gradphi[i][2];
                    }
                    

                    Dune::FieldVector<double,dim> gradp(0.0),flux(0.0);
                    
                    G.mv(e,gradp); // compute pressure gradient  grad(p) = gradphi * p
                    
                    Kij.mv(gradp,flux); // Compute flux = - Perm * G
                    
                    Dune::FieldVector<double,4> residual(0.0);
                    
                    G.mtv(flux,residual); // Compute residual vector res = - G' * Perm * G * p

                    
                    double eta = std::sqrt(dot(e,residual));
                    
                    // geometric weight
                    RF factor = it->weight() * eg.geometry().integrationElement(it->position());
                    
                    
                    r.accumulate(lfsv,0,eta * factor);
                    
                    
                } // end for each quadrature point
                
            } // end alpha_volume
            
            
        private:
            
            const LE& le;
            const V& pc;
            const GFS& gfs;
            const GV& gv;
            int intorder;
            
        };
        
        class QoI_pressure_value : public NumericalJacobianApplyVolume<QoI_pressure_value>,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<QoI_pressure_value >
        {
        public:
            // pattern assembly flags
            enum { doPatternVolume = true };
            
            // residual assembly flags
            enum { doAlphaVolume = true };
            
            QoI_pressure_value(Dune::FieldVector<double,3>& x0_, Dune::FieldVector<double,3>& ell_, int intorder_=2)
            : intorder(intorder_), x0(x0_), ell(ell_)
            {}
            
            // volume integral depending on test and ansatz functions
            template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
            void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
            {
                // dimensions
                const int dim = 3;
                const int dimw = 3;
                
                const unsigned int nodes_per_element = lfsv.size();
                
                // domain and range field type
                typedef typename LFSV::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::DomainFieldType DF;
                typedef typename R::value_type RF;
                typedef typename LFSV::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;
                typedef typename LFSV::Traits::SizeType size_type;
                
                typedef typename LFSV::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::RangeType RangeType;
                
                // select quadrature rule
                GeometryType gt = eg.geometry().type();
                const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,intorder);
                
                double vol = 0.0;
                
                Dune::FieldVector<double,4> res(0.0);
                
                // Loop over quadrature points
                for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
                {
                    
                    std::vector<RangeType> phi_p(lfsu.size());
                    lfsu.finiteElement().localBasis().evaluateFunction(it->position(),phi_p);
                    
                    std::vector<RangeType> phi(lfsv.size());
                    lfsv.finiteElement().localBasis().evaluateFunction(it->position(),phi);
                    
                    
                    // Compute Pressure at Integration Point
                    double p = 0.0;
                    Dune::FieldVector<double,dim> x_ip(0.0);
                    
                    for (int i = 0; i < lfsu.size(); i++)
                    {
                        p += x(lfsu,i) * phi_p[i];
                        for (int j = 0; j < dim; j++){
                            x_ip[j] += eg.geometry().corner(i)[j] * phi_p[i];
                        }
                    }
                    
                    double f = 0.0;
                    
                    for (int i = 0; i < dim; i++){
                        f += (x_ip[i] - x0[i]) * (x_ip[i] - x0[i]) / ell[0];
                    }
                    
                    f = (1.0)/(std::pow(M_PI*ell[0],1.5)) * std::exp(-f);
                    
                    // geometric weight
                
                    
                    RF factor = it->weight() * eg.geometry().integrationElement(it->position());
                    
                    
                    for (int i = 0; i < lfsv.size(); i++){
                        
                        r.accumulate(lfsv,i,p * f * factor * phi[i]);
                        
                        
                    }

                    
                } // end for each quadrature point

      
                
            } // end alpha_volume
            
            
        private:
        
            const Dune::FieldVector<double,3>& x0;
            const Dune::FieldVector<double,3>& ell;
            int intorder;
            
        };


template<typename GV, typename LE, bool dual_problem>
class error_estimator_residual_based : public NumericalJacobianApplyVolume<error_estimator_residual_based<GV,LE,dual_problem>>,
public FullVolumePattern,
public LocalOperatorDefaultFlags,
public InstationaryLocalOperatorDefaultMethods<double>,
public NumericalJacobianVolume<error_estimator_residual_based<GV,LE,dual_problem> >
{
public:
    // pattern assembly flags
    enum { doPatternVolume = false };
    enum { doPatternSkeleton = false };
    
    // residual assembly flags
    enum { doAlphaVolume  = true };
    enum { doAlphaSkeleton  = true };
    
    //! constructor: pass parameter object
    error_estimator_residual_based (GV& gv_,LE& le_)
    : le(le_), gv(gv_)
    {}
    
    // volume integral depending on test and ansatz functions
    template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
    {
        // domain and range field type
        typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename LFSU::Traits::SizeType size_type;
        
        // dimensions
        const int dim = EG::Geometry::mydimension;
        const int intorder = 2*lfsu.finiteElement().localBasis().order();
        
        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);
        
        // loop over quadrature points
        RF sum(0.0);
        for (const auto& ip : rule)
        {
            // evaluate basis functions
            std::vector<RangeType> phi(lfsu.size());
            lfsu.finiteElement().localBasis().evaluateFunction(ip.position(),phi);
            
            // evaluate u
            Dune::FieldVector<double,dim> x_global(0.0);
            for (size_type i=0; i<lfsu.size(); i++)
                x_global += eg.geometry().corner(i) * phi[i];
            
            // evaluate right hand side parameter function
            double f = le.evaluateF(x_global,dual_problem);
            
            // integrate f^2
            RF factor = ip.weight() * eg.geometry().integrationElement(ip.position());
            sum += f*f * factor;
        }
        
        // accumulate cell indicator
        DF h_T = diameter(eg.geometry());
        r.accumulate(lfsv,0,h_T*h_T*sum);
    }
    
    
    // skeleton integral depending on test and ansatz functions
    // each face is only visited ONCE!
    template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_skeleton (const IG& ig,
                         const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                         const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                         R& r_s, R& r_n) const
    {
        // domain and range field type
        typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename LFSU::Traits::SizeType size_type;
        
        // dimensions
        const int dim = IG::dimension;
        
        // get cell entities from both sides of the intersection
        auto inside_cell = ig.inside();
        auto outside_cell = ig.outside();
        
        // evaluate permeability tensors
        const Dune::FieldVector<DF,dim>&
        inside_local = Dune::ReferenceElements<DF,dim>::general(inside_cell.type()).position(0,0);
        const Dune::FieldVector<DF,dim>&
        outside_local = Dune::ReferenceElements<DF,dim>::general(outside_cell.type()).position(0,0);
        
        
        // Evaluate elasticity tensor (assumes it is constant over a single element)
        Dune::FieldMatrix<double,3,3> K_s(0.0), K_n(0.0);
        
        const typename GV::IndexSet& is(gv.indexSet());
        
        le.template evaluateTensor(is.index(inside_cell),K_s);
        le.template evaluateTensor(is.index(outside_cell),K_n);
        
        // select quadrature rule
        const int intorder = 2*lfsu_s.finiteElement().localBasis().order();
        Dune::GeometryType gtface = ig.geometryInInside().type();
        const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);
        
        // transformation
        typename IG::Entity::Geometry::JacobianInverseTransposed jac;
        
        // tensor times normal
        const Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();
        Dune::FieldVector<RF,dim> Kn_F_s;
        K_s.mv(n_F,Kn_F_s);
        Dune::FieldVector<RF,dim> Kn_F_n;
        K_n.mv(n_F,Kn_F_n);
        
        // loop over quadrature points and integrate normal flux
        RF sum(0.0);
        for (const auto& ip : rule)
        {
            // position of quadrature point in local coordinates of elements
            Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().global(ip.position());
            Dune::FieldVector<DF,dim> iplocal_n = ig.geometryInOutside().global(ip.position());
            
            // evaluate gradient of basis functions
            std::vector<JacobianType> gradphi_s(lfsu_s.size());
            lfsu_s.finiteElement().localBasis().evaluateJacobian(iplocal_s,gradphi_s);
            std::vector<JacobianType> gradphi_n(lfsu_n.size());
            lfsu_n.finiteElement().localBasis().evaluateJacobian(iplocal_n,gradphi_n);
            
            // transform gradients of shape functions to real element
            jac = inside_cell.geometry().jacobianInverseTransposed(iplocal_s);
            std::vector<Dune::FieldVector<RF,dim> > tgradphi_s(lfsu_s.size());
            for (size_type i=0; i<lfsu_s.size(); i++) jac.mv(gradphi_s[i][0],tgradphi_s[i]);
            jac = outside_cell.geometry().jacobianInverseTransposed(iplocal_n);
            std::vector<Dune::FieldVector<RF,dim> > tgradphi_n(lfsu_n.size());
            for (size_type i=0; i<lfsu_n.size(); i++) jac.mv(gradphi_n[i][0],tgradphi_n[i]);
            
            // compute gradient of u
            Dune::FieldVector<RF,dim> gradu_s(0.0);
            for (size_type i=0; i<lfsu_s.size(); i++)
                gradu_s.axpy(x_s(lfsu_s,i),tgradphi_s[i]);
            Dune::FieldVector<RF,dim> gradu_n(0.0);
            for (size_type i=0; i<lfsu_n.size(); i++)
                gradu_n.axpy(x_n(lfsu_n,i),tgradphi_n[i]);
            
            // integrate
            RF factor = ip.weight() * ig.geometry().integrationElement(ip.position());
            RF jump = (Kn_F_s*gradu_s)-(Kn_F_n*gradu_n);
            sum += 0.25*jump*jump*factor;
        }
        
        // accumulate indicator
        // DF h_T = diameter(ig.geometry());
        DF h_T = std::max(diameter(inside_cell.geometry()),diameter(outside_cell.geometry()));
        r_s.accumulate(lfsv_s,0,h_T*sum);
        r_n.accumulate(lfsv_n,0,h_T*sum);
    }
    
    
    
private:
    LE& le;  // two phase parameter class
    const GV& gv;
    
    template<class GEO>
    typename GEO::ctype diameter (const GEO& geo) const
    {
        typedef typename GEO::ctype DF;
        DF hmax = -1.0E00;
        const int dim = GEO::coorddimension;
        for (int i=0; i<geo.corners(); i++)
        {
            Dune::FieldVector<DF,dim> xi = geo.corner(i);
            for (int j=i+1; j<geo.corners(); j++)
            {
                Dune::FieldVector<DF,dim> xj = geo.corner(j);
                xj -= xi;
                hmax = std::max(hmax,xj.two_norm());
            }
        }
        return hmax;
    }
    
};



        
    }
}





#endif