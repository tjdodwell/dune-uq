//
//  linearelasticity.hh
//  
//
//  Created by Tim Dodwell on 01/12/2015.
//
//

#ifndef linearelasticity_h
#define linearelasticity_h

#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>

#include "defaultimp.hh"
#include "pattern.hh"
#include "flags.hh"
#include "idefault.hh"

namespace Dune {
    namespace PDELab {
        
        template<typename COEFF, int dofel>
        class linearelasticity : public NumericalJacobianApplyVolume<linearelasticity<COEFF,dofel>>,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>
        {
        public:
            // pattern assembly flags
            enum { doPatternVolume = true };
            
            // residual assembly flags
            enum { doAlphaVolume = true };
            
            linearelasticity (const COEFF& z_, int intorder_=2)
            : z(z_), intorder(intorder_)
            {}

            // volume integral depending on test and ansatz functions
            template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
            void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
            {
                // dimensions
                const int dim = 2;
                const int dimw = 2;
                
                // extract local function spaces
                typedef typename LFSU::template Child<0>::Type LFSU_U1;
                typedef typename LFSU::template Child<1>::Type LFSU_U2;
                
                
                const LFSU_U1& lfsu_u1 = lfsu.template child<0>();
                const LFSU_U2& lfsu_u2 = lfsu.template child<1>();
                
                const unsigned int nodes_per_element = lfsu_u1.size();
                
                // domain and range field type
                typedef typename LFSU_U1::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::DomainFieldType DF;
                typedef typename R::value_type RF;
                typedef typename LFSU_U1::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;
                typedef typename LFSU_U1::Traits::SizeType size_type;
                
                // select quadrature rule
                GeometryType gt = eg.geometry().type();
                const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,intorder);
                

                // Loop over quadrature points
                for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
                {
                    
                    // Evaluate Jacobian
                    std::vector<JacobianType> js(nodes_per_element);
                    lfsu_u1.finiteElement().localBasis().evaluateJacobian(it->position(),js);
                    
                    // Transform gradient to real element
                    const typename EG::Geometry::JacobianInverseTransposed jac = eg.geometry().jacobianInverseTransposed(it->position());
                    std::vector<Dune::FieldVector<RF,dim> > gradphi(nodes_per_element);
                    for (int i=0; i < nodes_per_element; i++)
                    {
                        gradphi[i] = 0.0;
                        jac.umv(js[i][0],gradphi[i]);
                    }
                    
                    /// Compute B - such that Strain = B * d
                    Dune::FieldMatrix<double,3,dofel> B(0.0);
                    
                    Dune::FieldVector<double,dofel> Strain(0.0), Stress(0.0);
                    
                    for (int i = 0; i < nodes_per_element; i++)
                    {
                        B[0][i] = gradphi[i][0]; // E11
                        B[1][i + nodes_per_element] = gradphi[i][1]; // E22
                        B[2][i] = gradphi[i][1];     B[2][i + nodes_per_element]  = gradphi[i][0]; // E12
                    }
                    
                    // Evaluate elasticity tensor at Integration Point
                    Dune::FieldMatrix<double,3,3> A = z.evaluateTensor(it->position());
                    
                    // Compute residual - B' * A * B * u
                    FieldVector<double,dofel> residual(0.0), u(0.0);
                    
                    for (int i=0; i<nodes_per_element; i++){
                        u[i] = x(lfsu_u1,i);
                        u[i + nodes_per_element] = x(lfsu_u2,i);
                    }
                    
                    B.mv(u,Strain);
                    
                    A.mv(Strain,Stress);
                    
                    B.mtv(Stress,residual);
                    
                    // geometric weight
                    RF factor = it->weight() * eg.geometry().integrationElement(it->position());
                    
                    
                    for (int i=0; i < nodes_per_element; i++){
                        r.accumulate(lfsu_u1,i,residual[i] * factor);
                        r.accumulate(lfsu_u2,i,residual[i + nodes_per_element] * factor);
                    }
                    
                    
                } // end for each quadrature point
                
            } // end alpha_volume
            
            
            
            
        private:
            
            const COEFF& z;
            int intorder;
            
        };
        
    }
}




#endif /* linearelasticity_h */
