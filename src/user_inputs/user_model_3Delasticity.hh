//
//  user_model.hh
//  
//
//  Created by Tim Dodwell on 30/11/2015.
//
//

#ifndef user_model_h
#define user_model_h

#include <random>
#include "../UQ_functions/KLFunctions.h"
#include "../UQ_functions/general.hh"
#include "linearelasticity3D.hh"

// ------------------------------------------------------------------------
//             Dirichlet Boundary Conditions Class
// ------------------------------------------------------------------------
template<typename GV, typename RF>
class Dirichlet_BC :
public Dune::PDELab::AnalyticGridFunctionBase<
Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
Dirichlet_BC<GV,RF> >,
public Dune::PDELab::InstationaryFunctionDefaults
{
    
private:
    int dof;
    
public:
    
    typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
    typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, Dirichlet_BC<GV,RF>> BaseT;
    
    typedef typename Traits::DomainType DomainType;
    typedef typename Traits::RangeType RangeType;
    
    // Constructor
    Dirichlet_BC(const GV & gv) : BaseT(gv)
    {
    }
    
    template<typename I>
    bool isDirichlet(const I & ig,
                     const typename Dune::FieldVector<typename I::ctype, I::dimension-1> & x
                     ) const
    {
        Dune::FieldVector<double,3> xg = ig.geometry().global( x );
        bool answer = false;
        if (xg[0] < 1e-6 || xg[1] < 1e-6 || xg[0] > 1.0 - 1e-6 || xg[1] > 1.0 - 1e-6){  answer = true;  }
        return answer;
    }
    
    
    inline void evaluateGlobal(const DomainType & x, RangeType & u) const
    {
        u = 0.0;
        
        if ((x[0] > 1.0 - 1e-6) && dof == 1){ u = 0.1; }
        
    } // end inline function evaluateGlobal
    
    void setDof(int degree_of_freedom){
        dof = degree_of_freedom;
    }
}; // End of Dirichlet BC class


// Class which constructs
class COEFF
{
public:
    // Constructor for COEFF class
    COEFF(double L,int numKLmodes, double sigKL, double ellKL, const std::vector<double>& mat)
    : numKLmodes_(numKLmodes), sigKL_(sigKL), ellKL_(ellKL), mat_(mat)
    {
        xi.resize(numKLmodes_);
        
        // Define and Resize vector for KL modes definition
        int N = std::sqrt(numKLmodes) + 1;
        freq.resize(N); lam1D.resize(N); lam3D.resize(numKLmodes);
        mode_id_i.resize(numKLmodes);   
        mode_id_j.resize(numKLmodes);
        mode_id_k.resize(numKLmodes);
        
        Dune::Timer timer;
        
        KLExpansion(N,0.5 * L,sigKL_,ellKL_,freq);
        evaluate_eigenValues(ellKL_,lam1D,freq);
        
       
        construct_3D_eigenValues(lam1D, lam3D, mode_id_i, mode_id_j, mode_id_k);
        
        std::cout << "Karman-Loeve Expansion Computed - " << timer.elapsed() << " secs" << std::endl;

    };
    
    double inline evalPhi(double x,int i, double L){
    
    double phi = 0.0;
        
    double omega = freq[i];
        
    x -= L;
    
        double tmp = sqrt(L + std::sin(2 * omega * L) / (2 * omega));
    
    if (i % 2 == 0){ phi = std::cos(omega * x) / tmp;}
    else { phi = std::sin(omega * x) / tmp; }
    
        return phi;
    
    }


    template <typename GV>
    void inline computeTensor(GV& gv, double L)
    {

        typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;
        
        const typename GV::IndexSet& is(gv.indexSet());
        
        Cijkl.resize(is.size(0));
        

        for (ElementIterator eg = gv.template begin<0>(); eg!=gv.template end<0>(); ++eg)
        { // loop through each element
            int id = is.index(*eg); // Get element id
            
            int numNodes = eg.geometry().corners();
            
            Dune::FieldVector<double,3> x(0.0);
            
            for (int i = 0; i < numNodes; i++){
                x += eg.geometry().corner(i);
            }
            
            Cijkl[id] = evaluateMatrix(x,L);
        }

    }

    Dune::FieldMatrix<double,6,6> evaluateMatrix(const Dune::FieldVector<double,3> x,double L)
    {
        double E = mat_[0];
        for (int j = 0; j < numKLmodes_; j++){
            E += std::sqrt(lam3D[j]) * evalPhi(x[0],mode_id_i[j],L) * evalPhi(x[1],mode_id_j[j],L) * evalPhi(x[2],mode_id_k[j],L) * xi[j];
        }
        
        double E11 = E, E22 = E, nu12 = mat_[2], G12 = E / (2.0 * ( 1.0 + nu12));
        double nu21 = nu12 * (E22 / E11);
        double factor = 1.0 - nu12 * nu21;
        Dune::FieldMatrix<double,6,6> Q(0.0);
        Q[0][0] = E11 / factor; Q[0][1] = nu12 * E22 / factor; Q[0][2] = nu12 * E22 / factor;
        Q[1][0] = Q[0][1]; Q[1][1] = E22 / factor; Q[1][2] = nu12 * E22 / factor;
        Q[2][0] = nu12 * E22 / factor; Q[2][1] = nu12 * E22 / factor; Q[2][2] = Q[1][1] = E22 / factor;;
        Q[3][3] = G12;
        Q[4][4] = G12;
        Q[5][5] = G12;
        return Q;
    }

    void inline evaluateTensor(int id, Dune::FieldMatrix<double,6,6>& Q) const{
        Q = Cijkl[id];
    }

    
    void inline user_random_field()
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::normal_distribution<double> randn(0.0,sigKL_);
        
        std::fill(xi.begin(), xi.end(), 0.0);
        
        for (int j = 0; j < numKLmodes_; j++){
            xi[j] = randn(gen);
        }
   
    } // end user_random_field
    
    
private:
    int numPly_;
    int numKLmodes_;
    double sigKL_, defectProbability_, ellKL_;
    std::vector<double> xi;
    std::vector<double> freq, lam1D, lam3D, mat_;
    std::vector<int> mode_id_i, mode_id_j, mode_id_k;
    std::vector<Dune::FieldMatrix<double,6,6>> Cijkl;
    
};


template<typename GRID>
class MODEL{
    
public:
    
    
    const int numMode1D = 4;
    int numKLmodes = 100;
    
    double sigKL = 1.0;
    double ellKL = 2.0;
    
    Dune::FieldVector<double,3> L;

    int MaxIt = 5000;
    int Verbosity = 1;
    double tolerance = 1e-4;
    
    GRID& grid_;
    
    std::vector<double> mat;
    
    // Constructor for MODEL CLASS
    MODEL(GRID& grid, Dune::FieldVector<double,3>& L_):grid_(grid), L(L_){
    
        mat.resize(4);
        mat[0] = 50.0;
        mat[1] = 0.0;
        mat[2] = 0.25;
        mat[3] = 0.0;
    
    };
    
    template <typename COEFF, typename GV, typename GFS, typename X, typename X0, int dofel>
    double get_eta(const GFS& gfs, const GV& gv,const COEFF& z, const X& x0, const X& x1, const X0& eta)
    const {
        
        const int dim = 3;
        
        const int intorder = 2;
        
        typedef typename GV::Traits::template Codim<0>::Iterator EG; // Iterator over grid view
        
        const typename GV::IndexSet& is(gv.indexSet());
        
        //--------------------------------------------------------------
        // Make local function space for Piecewise Linear Finite Element
        //--------------------------------------------------------------
        
        typedef Dune::PDELab::LocalFunctionSpace<GFS> CLFS;
        CLFS clfs(gfs);
        typedef Dune::PDELab::LFSIndexCache<CLFS> CLFSCache;
        CLFSCache clfsCache(clfs);
        std::vector<double> xlc_c(clfs.maxSize());
        std::vector<double> xlc_f(clfs.maxSize());
        
        typedef typename X::template ConstLocalView<CLFSCache> XView;
        XView xcView(x0);
        XView xfView(x1);
        
        typedef typename CLFS::template Child<0>::Type LFS1;
        typedef typename CLFS::template Child<1>::Type LFS2;
        typedef typename CLFS::template Child<2>::Type LFS3;
        
        typedef typename LFS1::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::DomainFieldType DF;
        
        std::vector<double> err_measure(eta.flatsize());
        
        // loop over grid view
        for (EG eg = gfs.gridView().template begin<0>(); eg!=gfs.gridView().template end<0>(); ++eg)
        {
            
            int id = is.index(*eg); // get element id
            
            // Bind solution x to local element
            clfs.bind(*eg);
            clfsCache.update();
            
            xcView.bind(clfsCache);
            xcView.read(xlc_c);
            
            xfView.bind(clfsCache);
            xfView.read(xlc_f);
            
            const LFS1& lfs1 = clfs.template child<0>();
            const LFS2& lfs2 = clfs.template child<1>();
            const LFS3& lfs3 = clfs.template child<2>();
            
            int nodel = lfs1.size();
            
            err_measure[id] = 0.0;
            
            // Define Quadrature points with element eg
            Dune::GeometryType gt = eg.geometry().type();
            const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);
            
            for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
            {
                std::vector<double> phi(nodel);
                lfs1.finiteElement().localBasis().evaluateFunction(it.position(),phi);
                
                // evaluate u
                Dune::FieldVector<double,dim> u_c(0.0), u_f(0.0);
                
                for (unsigned int i=0; i < nodel; i++){
                    u_c[0] += xlc_c(lfs1,i) * phi[i]; u_f[0] += xlc_f(lfs1,i) * phi[i];
                    u_c[1] += xlc_c(lfs2,i) * phi[i]; u_f[0] += xlc_f(lfs2,i) * phi[i];
                    u_c[2] += xlc_c(lfs3,i) * phi[i]; u_f[0] += xlc_f(lfs3,i) * phi[i];
                }
                
                double err = 0.0;
                
                for (int i = 0; i < dim; i++){ err += std::abs(u_c[i] - u_f[i]); }
                
                double factor = it->weight() * eg.geometry().integrationElement(it->position());
                
                err_measure[id] += err * factor;
                
            } // End for loop through each quadrature point
            
        } // End loop through each element
        
        for (unsigned int i=0; i<eta.flatsize(); i++)
            Dune::PDELab::istl::raw(eta)[i] = err_measure[i];
        
        
    }
    
    template <typename COEFF, typename GV, typename GFS, typename X, int dofel>
    double user_QoI(const GFS& gfs,const X& x, const GV& gv,const COEFF& z)
    const {
        
        const int dim = 3;
        const int intorder = 1;
        typedef typename GV::Traits::template Codim<0>::Iterator EG;
        
        // make local function space
        typedef Dune::PDELab::LocalFunctionSpace<GFS> CLFS;
        CLFS clfs(gfs);
        typedef Dune::PDELab::LFSIndexCache<CLFS> CLFSCache;
        CLFSCache clfsCache(clfs);
        std::vector<double> xlc(clfs.maxSize());
        
        typedef typename X::template ConstLocalView<CLFSCache> XView;
        XView xView(x);
        
        typedef typename CLFS::template Child<0>::Type LFS1;
        typedef typename LFS1::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFS1::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::JacobianType JacobianType;
        
        typedef typename GV::IndexSet IndexSet;
        
        Dune::FieldMatrix<double,6,6> Cijkl(0.0);
        
        const typename GV::IndexSet& is(gv.indexSet());
        

        
        double Volume = 0;
        double emacro = 0.0;

        double smacro = 0.0;
        
        
        // loop over grid view
        for (EG eg = gfs.gridView().template begin<0>();
             eg!=gfs.gridView().template end<0>(); ++eg)
        {
            // bind solution x to local element
            clfs.bind(*eg);
            clfsCache.update();
            xView.bind(clfsCache);
            xView.read(xlc);
            
            const LFS1& lfs1 = clfs.template child<0>();
         
            int nodes_per_element = lfs1.size();
            
            // Define Quadrature points with element eg
            Dune::GeometryType gt = eg.geometry().type();
            const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);
            
            z.template evaluateTensor(is.index(*eg),Cijkl);
            
            for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
            {
                // Evaluate Jacobian
                std::vector<JacobianType> js(nodes_per_element);
                lfs1.finiteElement().localBasis().evaluateJacobian(it->position(),js);
                
                // Transform gradient to real element
                const typename EG::Geometry::JacobianInverseTransposed jac = eg.geometry().jacobianInverseTransposed(it->position());
                std::vector<Dune::FieldVector<double,dim> > gradphi(nodes_per_element);
                for (int i=0; i < nodes_per_element; i++){
                    gradphi[i] = 0.0;
                    jac.umv(js[i][0],gradphi[i]);
                }
                
                /// Compute B - such that Strain = B * d
                Dune::FieldMatrix<double,6,dofel> B(0.0);
                for (int i = 0; i < nodes_per_element; i++)
                {
                    B[0][i] = gradphi[i][0]; // E11
                    B[1][i + nodes_per_element] = gradphi[i][1]; // E22
                    B[2][i + 2*nodes_per_element] = gradphi[i][2]; // E22
                    B[3][i + nodes_per_element] = gradphi[i][2]; B[3][i + 2*nodes_per_element] = gradphi[i][1]; // E23
                    B[4][i] = gradphi[i][2];     B[4][i + 2*nodes_per_element] = gradphi[i][0]; // E13
                    B[5][i] = gradphi[i][1];     B[5][i + nodes_per_element]  = gradphi[i][0]; // E12
                    
                }
                
                Dune::FieldVector<double,6> e(0.0), s(0.0);
                
                B.mv(xlc,e); //	 e = B * x
                
                Cijkl.mv(e,s); // s = Cijkl * e
                
                double factor = it->weight() * eg.geometry().integrationElement(it->position());
                
                emacro += e[0] * factor;
                smacro += s[0] * factor;
                
                Volume += factor;
                
            } // End for loop through each quadrature point
            
        } // End loop through each element
        
        // Instantiate finite element maps
    
        double Q = smacro / emacro;
        
        
        return Q;
    }
    
    double inline user_adaptive_fem_driver(int l, COEFF& z) const{
        
        using Dune::PDELab::Backend::native;
        
        std::string gridName = "grids/test.msh";
    
        typedef Dune::UGGrid<3> UGGRID;
        UGGRID grid;
        Dune::GridFactory<UGGRID> factory(&grid);
        Dune::GmshReader<UGGRID>::read(factory,gridName,false);
        factory.createGrid();
        
        
        typedef typename UGGRID::LeafGridView LGV;
        LGV gv = grid.leafGridView(); // Get coarest grid
        
        z.computeTensor<LGV>(gv,0.5 * L[0]);
        
        typedef double RF;
        const int dim = LGV::Grid::dimension;
        typedef typename LGV::Grid::ctype Coord;
        
        typedef Dune::PDELab::PkLocalFiniteElementMap<LGV,Coord,RF,1> FEM;
        FEM fem(gv);
        
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        CON con;
        
        typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
        
        typedef Dune::PDELab::GridFunctionSpace<LGV, FEM, CON, VectorBackend> SCALAR_GFS;
        SCALAR_GFS dispU1(gv,fem,con); dispU1.name("U1");
        SCALAR_GFS dispU2(gv,fem,con); dispU2.name("U2");
        SCALAR_GFS dispU3(gv,fem,con); dispU3.name("U3");
        
        typedef typename SCALAR_GFS::template ConstraintsContainer<RF>::Type CC;
        
        
        typedef Dune::PDELab::CompositeGridFunctionSpace <VectorBackend,Dune::PDELab::LexicographicOrderingTag, SCALAR_GFS, SCALAR_GFS, SCALAR_GFS> GFS;
        GFS gfs(dispU1, dispU2, dispU3);
        
        // Make constraints map and initialize it from a function
        typedef typename GFS::template ConstraintsContainer<RF>::Type C;
        C cg;
        cg.clear();
        
        typedef Dirichlet_BC<LGV,RF> BC;
        BC U1_cc(gv), U2_cc(gv), U3_cc(gv);
        U1_cc.setDof(1);
        U2_cc.setDof(2);
        U3_cc.setDof(3);
        
        typedef Dirichlet_BC<LGV,RF> InitialDisp;
        InitialDisp u1(gv), u2(gv), u3(gv);
        u1.setDof(1);
        u2.setDof(2);
        u3.setDof(3);
        
        // Wrap scalar boundary conditions in to vector
        typedef Dune::PDELab::CompositeGridFunction<InitialDisp,InitialDisp,InitialDisp>  InitialSolution;
        InitialSolution initial_solution(u1,u2,u3);
   
        
        typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC>
        Constraints;
        Constraints constraints(U1_cc,U2_cc,U3_cc);
        
        Dune::PDELab::constraints(constraints,gfs,cg);
        
        
        //  Construct Linear Operator on FEM Space
        typedef Dune::PDELab::linearelasticity<COEFF,LGV,12> LOP;
        LOP lop(gv,z);
        
        typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
        MBE mbe(50); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().
        typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;
        GO go(gfs,cg,gfs,cg,lop,mbe);
        
        SCALAR_GFS scalar_gfs(gv,fem,con);

        typedef Dune::PDELab::GridOperator<SCALAR_GFS,SCALAR_GFS,LOP,MBE,RF,RF,RF,CC,CC> sGO;
        
        typedef typename sGO::Traits::Domain sV;
        sV sU1(scalar_gfs); sU1 = 0.0;
        sV sU2(scalar_gfs); sU2 = 0.0;
        sV sU3(scalar_gfs); sU3 = 0.0;
        
        
        // Make coefficent vector and initialize it from a function
        typedef typename GO::Traits::Domain V;
        V xf(gfs);
        
        xf = 0.0;
        
        V xc(gfs);
        Dune::PDELab::interpolate(initial_solution,gfs,xf);
        
        
        typedef Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO> SEQ_CG_AMG_SSOR;
        SEQ_CG_AMG_SSOR ls(MaxIt,0);
        Dune::PDELab::StationaryLinearProblemSolver<GO,SEQ_CG_AMG_SSOR,V> slp(go,ls,xf,tolerance,1e-99,0);
        
        // Construct Piecewise Constant FEM space for error estimator
        
        typedef Dune::PDELab::P0LocalFiniteElementMap<Coord,RF,dim> P0FEM;
        
        P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::simplex,dim));
        
        typedef Dune::PDELab::GridFunctionSpace<LGV,P0FEM,Dune::PDELab::NoConstraints,VectorBackend> P0GFS;
        
        typedef Dune::PDELab::EmptyTransformation NoTrafo;

        typedef typename Dune::PDELab::BackendVectorSelector<P0GFS,RF>:: Type V0;
        
        // Start adaptive loop
        
        double Qc = 0.0; double Qf = 0.0;
        
        for (int level = 0; level < l + 1; level++) // if l > 0
        {
        
            slp.apply(); // Compute xf
         
            
            Qf = user_QoI<COEFF,LGV,GFS,V,12>(gfs,xf,gv,z);
            
            
            if (l > 0 && level < l ){

                P0GFS p0gfs(gv,p0fem);
                V0 eta(p0gfs);

                if (level > 0) {
                    
                    eta = 0.0;
                    
                    std::cout << "Get Here " << std::endl;
                    
                    typedef Dune::PDELab::errorEstimator<V,LGV,GFS,12> ERRLOP;
                    ERRLOP errLop(gv,xc,gfs,1);
                    
                    typedef Dune::PDELab::GridOperator<GFS,P0GFS,ERRLOP,MBE,RF,RF,RF,NoTrafo,NoTrafo> ERRGO;
                    
                    ERRGO errgo(gfs,p0gfs,errLop,mbe);
                    
                    errgo.residual(xf,eta);
                        
                }
                else {  eta = 1.0;   }
                
                double alpha;
                double eta_alpha(0);     // refinement threshold
                double beta(0.0);        // coarsening fraction
                double eta_beta(0);      // coarsening threshold
                int verbose = 2;

                
                if (level == 0){ alpha = 1.0; }
                else { alpha = 0.4; }
                
                
                element_fraction( eta, alpha, beta, eta_alpha, eta_beta, verbose );
            
                mark_grid ( grid, eta, eta_alpha, eta_beta);
                
                typedef typename LGV::template Codim<0>::Iterator ElementLeafIterator;
                
                
                std::vector<int> marked(eta.flatsize());
                int ii = 0;
                for (ElementLeafIterator it = gv.template begin<0>();it!=gv.template end<0>(); ++it)
                {
                    marked[ii] = grid.getMark(*it);
                    std::cout << "Mark " << marked[ii] << std::endl;
                    ii +=1;
                }
                
                int nnodes = native(xf).size() / 3;
                
                for (int i = 0; i < nnodes; i++){
                    native(sU1)[i] = native(xf)[i];
                    native(sU2)[i] = native(xf)[i + nnodes];
                    native(sU3)[i] = native(xf)[i + 2 * nnodes];
                }
                
                //std::cout << "Number of Nodes before grid.adapt() = " << native(sU1).size(0) <<  std::endl;
                
                // prepare the grid for refinement
                
                adapt_grid( grid, scalar_gfs, sU1, 1 );
                
                // std::cout << "Number of Nodes after grid.adapt() = " << native(sU1).size(1) << std::endl;
                
                gfs.update(true);
                
                xc = xf;
                
                xf = V(gfs,0.0);
                
                
                
                
                
                
                
               /* typedef Dune::PDELab::GridFunctionSubSpace<GFS,Dune::TypeTree::TreePath<0> > U1GFS;
                U1GFS u1_gfs(gfs);
                
                
                typedef Dune::PDELab::DiscreteGridFunction<U1GFS,V> V_U1;
                
                typedef typename Dune::PDELab::BackendVectorSelector<SCALAR_GFS,RF>:: Type XX;
                
                XX v_u1(dispU1,0.0);
                
                //V_U1 u1(u1_gfs,xf);
            
                //adapt_grid( grid_, gfs, xf,1);
                
                adapt_grid( grid_, dispU1, v_u1,1);*/
                
                Dune::PDELab::constraints(constraints,gfs,cg);
                Dune::PDELab::interpolate(initial_solution,gfs,xf);
                
                Qc = Qf;  // Store quantity of interest of level l for next loop
                V xc(gfs,xf); // Store solution on level l on adapted grid for error estimator
                
            }
            
        }
        
        double Y = Qf - Qc;
        
        return Y;
        
        
    }

    
    double inline user_fem_driver(int l, COEFF& z) const{
        
        typedef typename GRID::LevelGridView LGV;
        LGV gv = grid_.levelGridView(l);
        
        typedef double RF;
        const int dim = LGV::Grid::dimension;
        typedef typename LGV::Grid::ctype Coord;
        
        z.computeTensor<LGV>(gv,0.5 * L[0]);
        
        typedef Dune::PDELab::PkLocalFiniteElementMap<LGV,Coord,RF,1> FEM;
        FEM fem(gv);
        
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        CON con;
        
        typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
        
        typedef Dune::PDELab::GridFunctionSpace<LGV, FEM, CON, VectorBackend> SCALAR_GFS;
        SCALAR_GFS dispU1(gv,fem,con); dispU1.name("U1");
        SCALAR_GFS dispU2(gv,fem,con); dispU2.name("U2");
        SCALAR_GFS dispU3(gv,fem,con); dispU3.name("U3");
        
        
        typedef Dune::PDELab::CompositeGridFunctionSpace <VectorBackend,Dune::PDELab::LexicographicOrderingTag, SCALAR_GFS, SCALAR_GFS, SCALAR_GFS> GFS;
        GFS gfs(dispU1, dispU2, dispU3);
        
        // Make constraints map and initialize it from a function
        typedef typename GFS::template ConstraintsContainer<RF>::Type C;
        C cg;
        cg.clear();
        
        typedef Dirichlet_BC<LGV,RF> BC;
        BC U1_cc(gv), U2_cc(gv), U3_cc(gv);
        U1_cc.setDof(1);
        U2_cc.setDof(2);
        U3_cc.setDof(3);

        typedef Dirichlet_BC<LGV,RF> InitialDisp;
        InitialDisp u1(gv), u2(gv), u3(gv);
        u1.setDof(1);
        u2.setDof(2);
        u3.setDof(3);
      
        // Wrap scalar boundary conditions in to vector
        typedef Dune::PDELab::CompositeGridFunction<InitialDisp,InitialDisp,InitialDisp>  InitialSolution;
        InitialSolution initial_solution(u1,u2,u3);


        
        typedef Dune::PDELab::CompositeConstraintsParameters<BC,BC,BC>
        Constraints;
        Constraints constraints(U1_cc,U2_cc,U3_cc);
        
        Dune::PDELab::constraints(constraints,gfs,cg);


        //  Construct Linear Operator on FEM Space
        typedef Dune::PDELab::linearelasticity<COEFF,LGV,12> LOP;
        LOP lop(gv,z);
    
        typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
        MBE mbe(50); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().
        typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;
        GO go(gfs,cg,gfs,cg,lop,mbe);

        // Make coefficent vector and initialize it from a function
        typedef typename GO::Traits::Domain V;
        V x0(gfs);
        x0 = 0.0;
        Dune::PDELab::interpolate(initial_solution,gfs,x0);


        typedef Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO> SEQ_CG_AMG_SSOR;
        SEQ_CG_AMG_SSOR ls(MaxIt,0);
        Dune::PDELab::StationaryLinearProblemSolver<GO,SEQ_CG_AMG_SSOR,V> slp(go,ls,x0,tolerance,1e-99,0);
        slp.apply();
        
        double Q = user_QoI<COEFF,LGV,GFS,V,12>(gfs,x0,gv,z);
        
        return Q;
    }

    

    
    
private:


    
    
};









#endif /* user_model_h */
