//
//  user_model.hh
//
//
//  Created by Tim Dodwell on 30/11/2015.
//
//

#ifndef user_model_h
#define user_model_h

#include <random>
#include "../UQ_functions/KLFunctions.h"
#include "../UQ_functions/general.hh"
#include "diffusion.hh"


// ------------------------------------------------------------------------
//             Dirichlet Boundary Conditions Class
// ------------------------------------------------------------------------
template<typename GV, typename RF>
class Dirichlet_BC :
public Dune::PDELab::AnalyticGridFunctionBase<
Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
Dirichlet_BC<GV,RF> >,
public Dune::PDELab::InstationaryFunctionDefaults
{
    
private:

    
public:
    
    typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
    typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, Dirichlet_BC<GV,RF>> BaseT;
    
    typedef typename Traits::DomainType DomainType;
    typedef typename Traits::RangeType RangeType;
    
    // Constructor
    Dirichlet_BC(const GV & gv) : BaseT(gv)
    {
    }
    
    template<typename I>
    bool isDirichlet(const I & ig,
                     const typename Dune::FieldVector<typename I::ctype, I::dimension-1> & x
                     ) const
    {
        Dune::FieldVector<double,3> xg = ig.geometry().global( x );
        bool answer = false;
        if (xg[0] < 1e-6 || xg[1] < 1e-6 || xg[0] > 1.0 - 1e-6 || xg[1] > 1.0 - 1e-6){  answer = true;  }
        return answer;
    }
    
    
    inline void evaluateGlobal(const DomainType & x, RangeType & u) const
    {
        u = 0.0;
        
        if ((x[0] > 1.0 - 1e-6)){ u = 1.0; }
        
    } // end inline function evaluateGlobal
    

}; // End of Dirichlet BC class

/*template<typename GV, typename RF>
class Dirac
public Dune::PDELab::AnalyticGridFunctionBase<
Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
Dirac<GV,RF> >,
public Dune::PDELab::InstationaryFunctionDefaults
{
    
public:
    
    typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
    typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, Dirichlet_BC<GV,RF>> BaseT;
    
    typedef typename Traits::DomainType DomainType;
    typedef typename Traits::RangeType RangeType;

    
    Dirac(std::vector<double> x0_,double ell_ = 1.0, int dim_ = 3): x0(x0_)
    {
        ell = ell_; dim = dim_;
    }
    
    inline void evaluate(const DomainType &x, RangeType & u) const
    {
        
        double d = 0.0;
        
        for (int i = 0; i < dim; i++){
            d += (x[i] - x0[i]) * (x[i] - x0[i]);
        }
        
        d = std::sqrt(d);
        
        if (d < ell){
            u = 1 / (2.0 * ell);
        }
    
    }
    
private:
    
        double ell;
        int dim;
        std::vector<double> x0;
};*/




#include<dune/common/fvector.hh>

#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>

//! \brief Parameter class selecting boundary conditions
class BCTypeParam
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
    //! Test whether boundary is Dirichlet-constrained
    template<typename I>
    bool isDirichlet(const I & intersection,
                     const Dune::FieldVector<typename I::ctype, I::dimension-1> & coord
                     ) const
    {
        Dune::FieldVector<typename I::ctype, I::dimension>
        xg = intersection.geometry().global( coord );
        
        if( xg[0] <1E-6 || xg[0]>1.0-1E-6 )
            return true; // no Dirichlet b.c. on the eastern boundary
        
        return false;  // Dirichlet b.c. on all other boundaries
    }
    
};

// Class which constructs
class COEFF
{
public:
    // Constructor for COEFF class
    COEFF(double L,int numKLmodes, double sigKL, double ellKL, const std::vector<double>& mat)
    : numKLmodes_(numKLmodes), sigKL_(sigKL), ellKL_(ellKL), mat_(mat)
    {
        xi.resize(numKLmodes_);
        
        // Define and Resize vector for KL modes definition
        int N = std::pow(numKLmodes,1.0/3.0) + 1;
        freq.resize(N); lam1D.resize(N); lam3D.resize(numKLmodes);
        mode_id_i.resize(numKLmodes);
        mode_id_j.resize(numKLmodes);
        mode_id_k.resize(numKLmodes);
        
        Dune::Timer timer;
        
        KLExpansion(N,0.5 * L,sigKL_,ellKL_,freq);
        evaluate_eigenValues(ellKL_,lam1D,freq);
        
        
        construct_3D_eigenValues(lam1D, lam3D, mode_id_i, mode_id_j, mode_id_k);
        
        std::cout << "Karman-Loeve Expansion Computed - " << timer.elapsed() << " secs" << std::endl;
        
    };
    
    double inline evalPhi(double x,int i, double L){
        
        double phi = 0.0;
        
        double omega = freq[i];
        
        x -= L;
        
        double tmp = sqrt(L + std::sin(2 * omega * L) / (2 * omega));
        
        if (i % 2 == 0){ phi = std::cos(omega * x) / tmp;}
        else { phi = std::sin(omega * x) / tmp; }
        
        return phi;
        
    }
    
    
    template <typename GV>
    void inline computeTensor(GV& gv, double L)
    {
        
        typedef typename GV::Traits::template Codim<0>::Iterator ElementIterator;
        
        const typename GV::IndexSet& is(gv.indexSet());
        
        Kij.resize(is.size(0));
        
        
        for (ElementIterator eg = gv.template begin<0>(); eg!=gv.template end<0>(); ++eg)
        { // loop through each element
            int id = is.index(*eg); // Get element id
            
            int numNodes = eg.geometry().corners();
            
            Dune::FieldVector<double,3> x(0.0);
            
            for (int i = 0; i < numNodes; i++){
                x += eg.geometry().corner(i);
            }
            
            x /= numNodes;
            
            Kij[id] = evaluateMatrix(x,L);
        }
        
    }
    
    Dune::FieldMatrix<double,3,3> evaluateMatrix(const Dune::FieldVector<double,3> x,double L)
    {
        double k = 3.0;
        for (int j = 0; j < numKLmodes_; j++){
            k += std::sqrt(lam3D[j]) * evalPhi(x[0],mode_id_i[j],L) * evalPhi(x[1],mode_id_j[j],L) * evalPhi(x[2],mode_id_k[j],L) * xi[j];
        }
        
        k = std::exp(k);
    
        Dune::FieldMatrix<double,3,3> Q(0.0);
        
        Q[0][0] = k;
        Q[1][1] = k;
        Q[2][2] = k;
        
        return Q;
    }
    
    void inline evaluateTensor(int id, Dune::FieldMatrix<double,3,3>& Q) const{
        Q = Kij[id];
    }
    
    double inline evaluateScalar(int id) const {
        return Kij[id][0][0];
    }
    
    
    void inline user_random_field()
    {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::normal_distribution<double> randn(0.0,1.0);
        
        std::fill(xi.begin(), xi.end(), 0.0);
        
        for (int j = 0; j < numKLmodes_; j++){
            xi[j] = sigKL_ * randn(gen);
        }
        
    } // end user_random_field
    
    double inline evaluateF(Dune::FieldVector<double,3>& xg, bool isDual) const {
        
        double f = 0.0;
        
        if (isDual){
            f = 0.0;
        }
        else{
            f = 1.0;
        }
        
        return f;
        
    }
    
    
private:
    
    int numPly_;
    int numKLmodes_;
    double sigKL_, defectProbability_, ellKL_;
    std::vector<double> xi;
    std::vector<double> freq, lam1D, lam3D, mat_;
    std::vector<int> mode_id_i, mode_id_j, mode_id_k;
    std::vector<Dune::FieldMatrix<double,3,3>> Kij;
    
};

template<typename GRID>
class MODEL{
    
public:

    int numKLmodes = 2000;
    
    double sigKL = 3.0;
    double ellKL = 1.0;
    
    Dune::FieldVector<double,3> L;
    
    int MaxIt = 5000;
    int Verbosity = 1;
    double tolerance = 1e-4;
    
    std::vector<double> mat;
    
    GRID& grid_;
    
    // Constructor for MODEL CLASS
    MODEL(GRID& grid, Dune::FieldVector<double,3>& L_):grid_(grid), L(L_){

        mat.resize(1); mat[0] = 0.0;
        
    };
    
    template <typename COEFF, typename GV, typename GFS, typename X>
    double user_QoI(const GFS& gfs,const X& x, const GV& gv,const COEFF& z)
    const {
        
        const int dim = 3;
        const int intorder = 1;
        typedef typename GV::Traits::template Codim<0>::Iterator EG;
        
        // make local function space
        typedef Dune::PDELab::LocalFunctionSpace<GFS> LFSU;
        LFSU lfsu(gfs);
        typedef Dune::PDELab::LFSIndexCache<LFSU> CLFSCache;
        CLFSCache clfsCache(lfsu);
        std::vector<double> p(lfsu.maxSize());
        
        typedef typename X::template ConstLocalView<CLFSCache> XView;
        XView xView(x);
        
        typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType;
        
        typedef typename GV::IndexSet IndexSet;
        
        Dune::FieldMatrix<double,3,3> Kij(0.0);
        
        const typename GV::IndexSet& is(gv.indexSet());
    
        double Volume = 0.0;
        double gradp_macro = 0.0;
        
        double flux_macro = 0.0;
        
        
        // loop over grid view
        for (EG eg = gfs.gridView().template begin<0>(); eg!=gfs.gridView().template end<0>(); ++eg)
        {
            // bind solution x to local element
            lfsu.bind(*eg);
            clfsCache.update();
            xView.bind(clfsCache);
            xView.read(p);
            
            const int nodel = lfsu.size();
            
            // Define Quadrature points with element eg
            Dune::GeometryType gt = eg.geometry().type();
            const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);
            
            z.template evaluateTensor(is.index(*eg),Kij);
            
            for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
            {
                // Evaluate Jacobian
                std::vector<JacobianType> js(nodel);
                lfsu.finiteElement().localBasis().evaluateJacobian(it->position(),js);
                
                // Transform gradient to real element
                const typename EG::Geometry::JacobianInverseTransposed jac = eg.geometry().jacobianInverseTransposed(it->position());
                std::vector<Dune::FieldVector<double,dim> > gradphi(nodel);
                for (int i=0; i < nodel; i++){
                    gradphi[i] = 0.0;
                    jac.umv(js[i][0],gradphi[i]);
                }
                
                Dune::FieldMatrix<double,dim,4> G(0.0);
                
                for (int i = 0; i < nodel; i++){
                    G[0][i] = gradphi[i][0];
                    G[1][i] = gradphi[i][1];
                    G[2][i] = gradphi[i][2];
                }
                
                Dune::FieldVector<double,dim> gradp(0.0),flux(0.0);
                
                G.mv(p,gradp); // compute pressure gradient  grad(p) = gradphi * p
                
                Kij.mv(gradp,flux); // Compute flux = - Perm * grad(p)
                
                double factor = it->weight() * eg.geometry().integrationElement(it->position());
                
                gradp_macro = gradp[0] * factor;
                
                flux_macro = flux[0] * factor;
                
                Volume += factor;
                
            } // End for loop through each quadrature point
            
        } // End loop through each element
        
        // Instantiate finite element maps
        
        double Q = flux_macro / gradp_macro;
        
        
        return Q;
    }
    
    double inline user_adaptive_fem_driver(int l, COEFF& z) const{
        
        using Dune::PDELab::Backend::native;
        
        std::string gridName = "grids/test.msh";
        
        GRID grid;
        Dune::GridFactory<GRID> factory(&grid);
        Dune::GmshReader<GRID>::read(factory,gridName,false);
        factory.createGrid();
        
        // assemble constraints
        
        // interpolate coefficient vector
        
        double Qf = 0.0;
        double Qc = 0.0;
        
        std::vector<double> p_temp;
        
        for (int i = 0; i < l + 1; i++)
        {
            
            Qc = Qf;
            
            typedef typename GRID::LeafGridView LGV;
            LGV gv = grid.leafGridView(); // Get coarest grid
            
            typedef double RF;
            const int dim = LGV::Grid::dimension;
            typedef typename LGV::Grid::ctype Coord;
            
            // <<<2>>> Make grid function space
            
            typedef Dune::PDELab::PkLocalFiniteElementMap<LGV,Coord,RF,1> FEM;
            FEM fem(gv);
            typedef Dune::PDELab::ConformingDirichletConstraints CON;     // constraints class
            typedef Dune::PDELab::ISTLVectorBackend<> VBE;
            typedef Dune::PDELab::GridFunctionSpace<LGV,FEM,CON,VBE> GFS;
            GFS gfs(gv,fem);
            gfs.name("solution");
            
            z.computeTensor<LGV>(gv,0.5 * L[0]);
            
            // <<<3>>> assemble constraints on this space
            BCTypeParam bctype; // boundary condition type
            typedef typename GFS::template ConstraintsContainer<RF>::Type CC;
            CC cc;
            Dune::PDELab::constraints( bctype, gfs, cc );
            
            using U = Dune::PDELab::Backend::Vector<GFS,RF>;
            U p(gfs,0.0);
            
            //  Make grid operator
            typedef Dune::PDELab::diffuse<COEFF,LGV> LOP;
            LOP lop(gv,z);
            typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
            MBE mbe(50);
            typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,CC,CC> GO;
            GO go(gfs,cc,gfs,cc,lop,mbe);

            // <<<4>>> make DOF vector
            typedef Dirichlet_BC<LGV,double> G;   // boundary value + extension
            G g(gv);
            Dune::PDELab::interpolate(g,gfs,p);
            
            // Select a linear solver backend
            typedef Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO> LS;
            LS ls(MaxIt,0);
            Dune::PDELab::StationaryLinearProblemSolver<GO,LS,U> slp(go,ls,p,tolerance,1e-99,0);
            
 
            // <<<8>>> Solve linear problem.
            slp.apply();
            
            Qf = user_QoI<COEFF,LGV,GFS,U>(gfs,p,gv,z);
            
            
            
            /*
            
            typedef Dune::PDELab::NoConstraints CON_NO;
            typedef Dune::PDELab::GridFunctionSpace<LGV,FEM,CON_NO,VBE> QOI_GFS;
            QOI_GFS qoi_gfs(gv,fem);
            qoi_gfs.name("local_QoI");
            typedef typename QOI_GFS::template ConstraintsContainer<RF>::Type CC_QOI;
            CC_QOI cc_qoi;
            
            Dune::FieldVector<double,dim> x0(0.6), ell(0.025);

            typedef Dune::PDELab::QoI_pressure_value QOI_LOP;
            QOI_LOP qoi_lop(x0,ell,4);
            
            typedef Dune::PDELab::GridOperator<GFS,QOI_GFS,QOI_LOP,MBE,RF,RF,RF,CC,CC_QOI> QOI_GO;
            QOI_GO qoi_go(gfs,cc,qoi_gfs,cc_qoi,qoi_lop,mbe);
            
            using U_QOI = Dune::PDELab::Backend::Vector<QOI_GFS,RF>;
            U_QOI qoi_nodes(qoi_gfs,0.0);
            
            qoi_go.residual(p,qoi_nodes);
            
            Qf = 0.0;
            
            int nnodes = native(qoi_nodes).size();
            
            for (int i = 0; i < nnodes; i++){
                Qf += native(qoi_nodes)[i];
            }
             */
           
            
            // <<<9>>> graphical output
            std::stringstream s;
            s << i;
            std::string iter;
            s >> iter;
            Dune::VTKWriter<LGV> vtkwriter(gv,Dune::VTK::conforming);
            Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,p);
            vtkwriter.write("adaptivity_"+iter,Dune::VTK::appendedraw);
            
            
            if (i < l){ // If not the last level the refine mesh
                
            // <<<A>>> Compute Estimated Error eta
                
            // Preparation: Define types for the computation of the error estimate eta.
            typedef Dune::PDELab::P0LocalFiniteElementMap<Coord,RF,dim> P0FEM;
            P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::simplex,dim));
            typedef Dune::PDELab::GridFunctionSpace<LGV,P0FEM,Dune::PDELab::NoConstraints,VBE> P0GFS;
            typedef Dune::PDELab::EmptyTransformation NoTrafo;
            using U0 = Dune::PDELab::Backend::Vector<P0GFS,RF>;
                
            P0GFS p0gfs(gv,p0fem);
            U0 eta(p0gfs);
                
            
                    
            eta = 0.0;
            typedef Dune::PDELab::error_estimator_residual_based<LGV,COEFF,false> ERRLOP;
            ERRLOP errLop(gv,z);
            typedef Dune::PDELab::GridOperator<GFS,P0GFS,ERRLOP,MBE,RF,RF,RF,NoTrafo,NoTrafo> ERRGO;
            ERRGO errgo(gfs,p0gfs,errLop,mbe);
            errgo.residual(p,eta);
             
            std::vector<double> a(gv.size(0));
                
            for (int i = 0; i < gv.size(0); i++){
                native(eta)[i] = sqrt(native(eta)[i]);
                a[i] = native(eta)[i];
            }
                
            
           Dune::VTKWriter<LGV> vtkwriter2(gv);
           vtkwriter2.addCellData(a, "eta");
           vtkwriter2.write("error_"+iter, Dune::VTK::appendedraw);
      
                

            // <<<B>>> Adapt the grid locally...
            
            
            double alpha(0.5);       // refinement fraction
            double eta_alpha(0);     // refinement threshold
            double beta(0.0);        // coarsening fraction
            double eta_beta(0);      // coarsening threshold
            int verbose = 0;
            
            Dune::PDELab::element_fraction( eta, alpha, beta, eta_alpha, eta_beta, verbose );
            
            Dune::PDELab::mark_grid( grid, eta, eta_alpha, 0.0 ,0 , 100, verbose);
            
            Dune::PDELab::adapt_grid( grid, gfs, p, 2 );
            
            // Store Solution in p_temp for next level
            p_temp.resize(native(p).size());
                
            for (int i = 0; i < p_temp.size(); i++){
                    p_temp[i] = native(p)[i];
            }
                
            } // end if i < l
            
        } // End for i
        
        double Y = Qf - Qc;
        
        return Y;
        
        
    }
    
    
    double inline user_fem_driver(int l, COEFF& z) const{
        
        typedef typename GRID::LevelGridView LGV;
        LGV gv = grid_.levelGridView(l);
        
        typedef double RF;
        const int dim = LGV::Grid::dimension;
        typedef typename LGV::Grid::ctype Coord;
        
        z.computeTensor<LGV>(gv,0.5 * L[0]);
        
        typedef Dune::PDELab::PkLocalFiniteElementMap<LGV,Coord,RF,1> FEM;
        FEM fem(gv);
        
        typedef Dune::PDELab::ConformingDirichletConstraints CON;
        
        typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
        
        typedef Dune::PDELab::GridFunctionSpace<LGV, FEM, CON, VectorBackend> GFS;
        GFS gfs(gv,fem); gfs.name("pressure");
        
        // Make constraints map and initialize it from a function
        typedef typename GFS::template ConstraintsContainer<RF>::Type C;
        C cg;
        cg.clear();
        
        BCTypeParam bctype;
        
        
        typedef Dirichlet_BC<LGV,RF> InitialDisp;
        InitialDisp p_initial(gv);

        
        Dune::PDELab::constraints(bctype,gfs,cg);
        
        
        //  Construct Linear Operator on FEM Space
        typedef Dune::PDELab::diffuse<COEFF,LGV> LOP;
        LOP lop(gv,z);
        
        typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
        MBE mbe(50); // Maximal number of nonzeroes per row can be cross-checked by patternStatistics().
        typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,C,C> GO;
        GO go(gfs,cg,gfs,cg,lop,mbe);
        
        // Make coefficent vector and initialize it from a function
        typedef typename GO::Traits::Domain V;
        V p(gfs,0.0);
        
        Dune::PDELab::interpolate(p_initial,gfs,p);
        
        typedef Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO> SEQ_CG_AMG_SSOR;
        SEQ_CG_AMG_SSOR ls(MaxIt,0);
        Dune::PDELab::StationaryLinearProblemSolver<GO,SEQ_CG_AMG_SSOR,V> slp(go,ls,p,tolerance,1e-99,0);
        
        slp.apply();
        
        
        double Q = user_QoI<COEFF,LGV,GFS,V>(gfs,p,gv,z);

        
        return Q;
    }
    
    
    
    
    
private:
    
    
    
    
};











#endif /* user_model_h */