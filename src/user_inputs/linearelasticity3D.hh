// -*- tab-width: 4; c-basic-offset: 2; indent-tabs-mode: nil -*-

#ifndef DUNE_PDELAB_LINEARELASTIC_HH
#define DUNE_PDELAB_LINEARELASTIC_HH

#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>

#include "defaultimp.hh"
#include "pattern.hh"
#include "flags.hh"
#include "idefault.hh"

namespace Dune {
    namespace PDELab {
        
        template<typename LE, typename GV, int dofel>
        class linearelasticity : public NumericalJacobianApplyVolume<linearelasticity<LE,GV,dofel>>,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<linearelasticity<LE,GV,dofel> >
        {
        public:
            // pattern assembly flags
            enum { doPatternVolume = true };
            
            // residual assembly flags
            enum { doAlphaVolume = true };
            
            linearelasticity (GV& gv_, LE& le_, int intorder_=2)
            : le(le_), intorder(intorder_), gv(gv_)
            {}
            
            // volume integral depending on test and ansatz functions
            template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
            void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
            {
                // dimensions
                const int dim = 3;
                const int dimw = 3;
                
                // extract local function spaces
                typedef typename LFSU::template Child<0>::Type LFSU_U1;
                typedef typename LFSU::template Child<1>::Type LFSU_U2;
                typedef typename LFSU::template Child<2>::Type LFSU_U3;
                
                const LFSU_U1& lfsu_u1 = lfsu.template child<0>();
                const LFSU_U2& lfsu_u2 = lfsu.template child<1>();
                const LFSU_U3& lfsu_u3 = lfsu.template child<2>();
                
                const unsigned int nodes_per_element = lfsu_u1.size();
                
                // domain and range field type
                typedef typename LFSU_U1::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::DomainFieldType DF;
                typedef typename R::value_type RF;
                typedef typename LFSU_U1::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;
                typedef typename LFSU_U1::Traits::SizeType size_type;
                
                // select quadrature rule
                GeometryType gt = eg.geometry().type();
                const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,intorder);
                
                // Evaluate elasticity tensor (assumes it is constant over a single element)
                Dune::FieldMatrix<double,6,6> C(0.0);
                Dune::FieldMatrix<double,dofel,dofel> K(0.0);


                // Unwrap solution for element
                FieldVector<double,dofel> u(0.0);
                for (int i=0; i<nodes_per_element; i++){
                        u[i] = x(lfsu_u1,i);
                        u[i + nodes_per_element] = x(lfsu_u2,i);
                        u[i + 2 * nodes_per_element] = x(lfsu_u3,i);
                }
                
                const typename GV::IndexSet& is(gv.indexSet());
    
                le.template evaluateTensor(is.index(eg.entity()),C);
                
                // Loop over quadrature points
                for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
                {

                    // Evaluate Jacobian
                    std::vector<JacobianType> js(nodes_per_element);
                    lfsu_u1.finiteElement().localBasis().evaluateJacobian(it->position(),js);
                    
                    // Transform gradient to real element
                    const typename EG::Geometry::JacobianInverseTransposed jac = eg.geometry().jacobianInverseTransposed(it->position());
                    std::vector<Dune::FieldVector<RF,dim> > gradphi(nodes_per_element);
                    for (int i=0; i < nodes_per_element; i++)
                    {
                        gradphi[i] = 0.0;
                        jac.umv(js[i][0],gradphi[i]);
                    }
                
                    
                    /// Compute B - such that Strain = B * d
                    Dune::FieldMatrix<double,6,dofel> B(0.0);
                    for (int i = 0; i < nodes_per_element; i++)
                    {
                        B[0][i] = gradphi[i][0]; // E11
                        B[1][i + nodes_per_element] = gradphi[i][1]; // E22
                        B[2][i + 2*nodes_per_element] = gradphi[i][2]; // E22
                        B[3][i + nodes_per_element] = gradphi[i][2]; B[3][i + 2*nodes_per_element] = gradphi[i][1]; // E23
                        B[4][i] = gradphi[i][2];     B[4][i + 2*nodes_per_element] = gradphi[i][0]; // E13
                        B[5][i] = gradphi[i][1];     B[5][i + nodes_per_element]  = gradphi[i][0]; // E12
                        
                    }

                    Dune::FieldVector<double,6> sig(0.0), eps(0.0);

                    B.mv(u,eps); // Compute Strain at integration point

                    C.mv(eps,sig); // Compute Stress at integration point

                    // Compute residual = K * U - checked
                    FieldVector<double,dofel> residual(0.0);

                    B.mtv(sig, residual);
                    
                    // geometric weight
                    RF factor = it->weight() * eg.geometry().integrationElement(it->position());
                
                    
                    for (int i=0; i < nodes_per_element; i++){
                        r.accumulate(lfsu_u1,i,residual[i] * factor);
                        r.accumulate(lfsu_u2,i,residual[i + nodes_per_element] * factor);
                        r.accumulate(lfsu_u3,i,residual[i + 2 * nodes_per_element] * factor);
                    }
                    
                    
                } // end for each quadrature point
                
            } // end alpha_volume
            
            
        private:
                        
            const LE& le;
            const GV& gv;
            int intorder;
            
        };
        
        
        template<typename V, typename GV, typename GFS, int dofel>
        class errorEstimator : public NumericalJacobianApplyVolume<errorEstimator<V,GV,GFS,dofel>>,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<double>,
        public NumericalJacobianVolume<errorEstimator<V,GV,GFS,dofel> >
        {
        public:
            // pattern assembly flags
            enum { doPatternVolume = true };
            
            // residual assembly flags
            enum { doAlphaVolume = true };
            
            errorEstimator (GV& gv_, V& xc_, GFS& gfs_, int intorder_=2)
            : xc(xc_), intorder(intorder_), gv(gv_), gfs(gfs_)
            {}
            
            // volume integral depending on test and ansatz functions
            template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
            void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
            {
                // dimensions
                const int dim = 3;
                const int dimw = 3;
                
                
                const typename GV::IndexSet& is(gv.indexSet());
                
                typedef Dune::PDELab::LocalFunctionSpace<GFS> CLFS;
                CLFS clfs(gfs);
                
                typedef Dune::PDELab::LFSIndexCache<CLFS> CLFSCache;
                CLFSCache clfsCache(clfs);
                std::vector<double> xlc_c(clfs.maxSize());
                
                
                typedef typename V::template ConstLocalView<CLFSCache> VView;
                VView xcView(xc);
                
                
                // Bind solution x to local element
                clfs.bind(eg.entity());
                clfsCache.update();
                
                xcView.bind(clfsCache);
                xcView.read(xlc_c);
                
                
                // extract local function spaces
                typedef typename LFSU::template Child<0>::Type LFSU_U1;
                typedef typename LFSU::template Child<1>::Type LFSU_U2;
                typedef typename LFSU::template Child<2>::Type LFSU_U3;
                
                const LFSU_U1& lfsu_u1 = lfsu.template child<0>();
                const LFSU_U2& lfsu_u2 = lfsu.template child<1>();
                const LFSU_U3& lfsu_u3 = lfsu.template child<2>();
                
                typedef typename CLFS::template Child<0>::Type LFS1;
                typedef typename CLFS::template Child<1>::Type LFS2;
                typedef typename CLFS::template Child<2>::Type LFS3;
                
                const LFS1& lfs1 = clfs.template child<0>();
                const LFS2& lfs2 = clfs.template child<1>();
                const LFS3& lfs3 = clfs.template child<2>();
                
                const unsigned int nodes_per_element = lfsu_u1.size();
                
                // domain and range field type
                typedef typename LFSU_U1::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::DomainFieldType DF;
                typedef typename R::value_type RF;
                typedef typename LFSU_U1::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;
                typedef typename LFSU_U1::Traits::SizeType size_type;
                
                typedef typename LFSU_U1::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::RangeType RangeType;

                
                // select quadrature rule
                GeometryType gt = eg.geometry().type();
                const QuadratureRule<DF,dim>& rule = QuadratureRules<DF,dim>::rule(gt,intorder);
                
                // Loop over quadrature points
                for (typename Dune::QuadratureRule<DF,dim>::const_iterator it = rule.begin(),endit = rule.end(); it != endit; ++it)
                {
                    
                    std::vector<RangeType> phi(nodes_per_element);
                    lfs1.finiteElement().localBasis().evaluateFunction(it->position(),phi);
                    
                    Dune::FieldVector<double,3> e(0.0);
                    
                    for (int i = 0; i < nodes_per_element; i++){
                        e[0] += (x(lfsu_u1,i) - xlc_c[i]) * phi[i];
                        e[1] += (x(lfsu_u2,i) - xlc_c[i + nodes_per_element]) * phi[i];
                        e[2] += (x(lfsu_u3,i) - xlc_c[i + 2 * nodes_per_element]) * phi[i];
                    }
                    
                    double eta = 0.0;
                    
                    for (int i = 0; i < 3; i++){
                        eta += std::abs(e[i]);
                    }
                    
                    
                    // geometric weight
                    RF factor = it->weight() * eg.geometry().integrationElement(it->position());
                    
                    
                    r.accumulate(lfsv,0,eta * factor);
                    
                    
                } // end for each quadrature point
                
            } // end alpha_volume
            
            
        private:
            
            const V& xc;
            const GFS& gfs;
            const GV& gv;
            int intorder;
            
        };

        
    }
}
#endif